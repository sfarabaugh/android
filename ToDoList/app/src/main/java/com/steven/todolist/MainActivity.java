package com.steven.todolist;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Map<String,String> websafeChars = new HashMap<String, String>()
    {{
        //put("key", "value");
            put(" ","%20");
            put("!","%21");
            put("\"","%22");
            put("#","%23");
            put("$","%24");
// percent should go here
            put("&","%26");
            put("'","%27");
            put("(","%28");
            put(")","%29");
            put("*","%2A");
            put("+","%2B");
            put(",","%2C");
            put("-","%2D");
            put(".","%2E");
            put("/","%2F");
            put(":","%3A");
            put(";","%3B");
            put("<","%3C");
            put("=","%3D");
            put(">","%3E");
            put("?","%3F");
            put("@","%40");
            put("[","%5B");
            put("\\","%5C");
            put("]","%5D");
            put("^","%5E");
            put("_","%5F");
            put("`","%60");
            put("{","%7B");
            put("|","%7C");
            put("}","%7D");
            put("~","%7E");
    }};
    public String wildHare = "";
    // URL for the server, might need to change to submit.py
    // [FUTURE] Settings pane for server
    public String serverURL = "http://technicalviking.com/cgi-bin/test.py?";
    public String serverResponse = "no response";
    // Logic for the id number for the app
    boolean first_load_completed;
    // #############################
    // ANDROID OS required function
    // handles the app on becoming active activity
    // #############################
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Access the shared preferences (player prefs)
        SharedPreferences sPrefs = getSharedPreferences("toDoListPrefs", 0);
        // Check if we have loaded before; false if DNE
        first_load_completed = sPrefs.getBoolean("first_load_complete", false);
        // Handle first launch
        if (!first_load_completed) {
            SharedPreferences.Editor editor = sPrefs.edit();
            // instaniate an id number in the 10^5s
            // We've saved before, now too.
            editor.putInt("id", 100000);
            editor.putBoolean("first_load_complete", true);
            // Save that ish
            editor.commit();
        }
        // Set the content to the layout in activity_main
        setContentView(R.layout.activity_main);
        // #############################
        // I found this on the internet and it keeps the app from crashing... ? unicorns here
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // #############################
        // Create the UI Stuff
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Spinner classSpinner = (Spinner) findViewById(R.id.classInput);
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(this,
                R.array.classes, android.R.layout.simple_spinner_item);
        classAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        classSpinner.setAdapter(classAdapter);
        santizeString("hello");
    }
    public String santizeString (String string) {
        Log.d("steven","input String"+string);
        String safeString = string;
        for (Map.Entry<String,String> entry : websafeChars.entrySet()){
            if (string.contains(entry.getKey())){
                safeString = safeString.replace(entry.getKey(), entry.getValue());
            }
        }
        Log.d("steven","output string"+safeString);
        return safeString;
    }
    // #############################
    // Button that creates our submitted json for the assignment
    // #############################
    public void createJSON(View view) {
        Log.d("steven", "createJSON, the button works");
        // Connect with the text fields and spinner
        EditText assignET = (EditText) findViewById(R.id.assignmentInput);
//        EditText dueDateET = (EditText) findViewById(R.id.dueDateInput);
        EditText guessTimeET = (EditText) findViewById(R.id.guessTimeInput);
        EditText notesET = (EditText) findViewById(R.id.notesInput);
        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        Spinner classSpinner = (Spinner) findViewById(R.id.classInput);
        // Extract text field value
        String assignmentTitle = santizeString(assignET.getText().toString());
        // Extract and conncantaonate date
        String month = Integer.toString(datePicker.getMonth()+1);
        String day = Integer.toString(datePicker.getDayOfMonth());
        String year = Integer.toString(datePicker.getYear());
        String hour = Integer.toString(timePicker.getHour());
        String minute = Integer.toString(timePicker.getMinute());
        String dueDate = month + "/" + day + "/"+ year + " " + hour + ":" + minute;

        Log.d("steven",dueDate);
        String guessTime = guessTimeET.getText().toString();
        String notes = santizeString(notesET.getText().toString());
        // Extract Spinner value
        String classTitle = santizeString(classSpinner.getSelectedItem().toString());
        // Create unix time string (Extract date value)
        // dateString has to exist outside of the try/catch... so it gets this date
        long dateString = 1481403600;
        try {
            dateString = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(dueDate + ":00").getTime() / 1000;
        } catch (java.text.ParseException e) {
            Toast.makeText(MainActivity.this, "Incorrect date time format MM/dd/yyyy HH:mm", Toast.LENGTH_LONG).show();
        }
        // data validation! yay!
        // [FUTURE] Make this hella better
        if (assignmentTitle.equals("") || guessTime.equals("")) {
            Toast.makeText(MainActivity.this, "ERROR: Add assign name and Estimated time", Toast.LENGTH_LONG).show();
        } else if (classTitle.equals("class")) {
            Log.d("steven", "pick a class");
            Toast.makeText(MainActivity.this, "ERROR: Choose a class", Toast.LENGTH_LONG).show();
        } else {
            // Validated data, connect with shared prefs, and grab the id. 404 in bad case
            SharedPreferences sharedPrefs = getSharedPreferences("toDoListPrefs", 0);
            Integer idNumber = sharedPrefs.getInt("id", 404);
            // #######################
            // OLD CODE
                // String JSONString = "toDoList_key_2_PROD{\"assignment\":\"" + assignmentTitle + "\",\"class\":\"" + classTitle + "\",\"dueDate\":\"" + dateString + "\",\"guessTime\":\"" + guessTime;
                // JSONString = JSONString + "\",\"notes\":\"" + notes + "\"";
                // JSONString += ",\"" + "status\":\"new\"";
                    // // change the id string for reasons
                // JSONString += ",\"" + "id\":\"FAMILY_" + idNumber.toString() + "\"";
                // JSONString += ",\"" + "actualTime\":\"0\"";
                // JSONString += "}";
            // #######################
            // Listen, the internet tells me to do things and I do them
            try {
                String ws_assignmentTitle = URLEncoder.encode(assignmentTitle, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            // New JSON string with test.py (should seemlessly transalte to submit.py
            String JSONString = "assignment="+assignmentTitle+"&classTitle="+classTitle+"&dueDate="+dateString+"&guessTime="+guessTime;
            JSONString += "&notes="+notes+"&status=new"+"&id=TEST_"+idNumber.toString()+"&actualTime=0";
            // Logging is important
            Log.d("Steven", assignmentTitle);
            Log.d("Steven", dueDate);
            Log.d("Steven", classTitle);
            Log.d("Steven", guessTime);
            Log.d("Steven", notes);
            Log.d("steven", "STRING SENT:  " + JSONString);
            Toast.makeText(MainActivity.this, JSONString, Toast.LENGTH_SHORT).show();
            // Payload generated, SEND IT!
            sendJSON(JSONString);
            // Increment the id number, connect with shared prefs, and save that ish
            idNumber++;
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt("id",idNumber);
            editor.commit();
        }
    }
    // Executed by createJSON
    public boolean sendJSON(String JSONString) {
        // Not sure this is super 100% correct way to start the async task. but it works so ¯\_(ツ)_/¯
        new DownloadFileAsync().execute(serverURL+JSONString);
        // Looking at this commented out line, pretty sure I fixed it
        // [FUTURE] CHECK IF THE ASYNC TASK IS WORKING
//        new DownloadFileAsync().doInBackground(serverURL+JSONString);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // This is commeneted out because as of 12/22/2016 the only option is reseting the ID
            // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        // Reset the ID # to 10^5
        if (id == R.id.action_settings) {
            SharedPreferences sharedPrefs = getSharedPreferences("toDoListPrefs", 0);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt("id",10000);
            editor.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // ASYNC TASK, BOIS
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            Log.d("steven","does preex run?");
            super.onPreExecute();
        }
        // Work horse
        @Override
        protected String doInBackground(String... aurl) {
            try {
                Log.d("steven", aurl[0]);
                // Creating the URL from the server+payload (not sure this is right)
                URL u = new URL(aurl[0]);
                Log.d("steven", "rekt1");
                // Create connection machinery
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                Log.d("steven", "rekt1");
                // POST
                c.setRequestMethod("POST");
                Log.d("steven", "rekt3");
                // Connect, yo
                c.connect();
                Log.d("steven", "rekt4");
                // grab the response
                wildHare = c.getInputStream().toString();
                Log.d("steven", c.getResponseMessage());
                serverResponse = c.getResponseMessage();
                serverResponse += Integer.toString(c.getResponseCode());
                // disconnect
                c.disconnect();
                Log.d("steven", "rekt5");
            } catch (Exception e) {
                Log.e("steven", "The connection threw an error", e);
            }
            return null;
        }
        @Override
        protected void onPostExecute(String unused) {
            //dismiss the dialog after the file was downloaded
            Log.d("steven","post execute");
            Toast.makeText(getBaseContext(), "Lynx submitted:   "+serverResponse, Toast.LENGTH_LONG).show();
            Toast.makeText(getBaseContext(), wildHare, Toast.LENGTH_LONG).show();
        }
    }
}